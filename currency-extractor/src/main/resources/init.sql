create table if not exists currency_history_realtime (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);

create table if not exists currency_history_minute (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);

create table if not exists currency_history_hour (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);

create table if not exists currency_history_day (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);

create table if not exists currency_history_week (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);


create table if not exists currency_history_month (
    a_id bigserial not null,
    a_time  timestamp not null,
    a_currency varchar not null,
    a_value double not null,
    PRIMARY KEY  (a_id, a_time)
);
