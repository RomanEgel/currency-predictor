package com.spbstu.currencyextractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyExtractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyExtractorApplication.class, args);
	}

}
