package com.spbstu.currencyextractor.startup;

import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.common.service.FluxKafkaSender;
import com.spbstu.currencyextractor.service.CurrencyExtractor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CurrencyExtractorStarter {
    private final CurrencyExtractor currencyExtractor;
    private final ScheduledExecutorService executorService;
    private final FluxKafkaSender<CurrencyDTO> kafkaSender;
    private static final Logger LOG = LoggerFactory.getLogger(CurrencyExtractorStarter.class);

    @EventListener(classes = {ContextRefreshedEvent.class})
    public void startCurrencyExtraction() {
        LOG.info("<-----START EXECUTOR SERVICE FOR CURRENCY EXTRACTION------->");
        executorService.scheduleWithFixedDelay(() -> kafkaSender.send(currencyExtractor.retrieveCurrency()
                .flatMapIterable(Function.identity())), 1, 3, TimeUnit.SECONDS);
    }
}