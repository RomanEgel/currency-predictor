package com.spbstu.currencyextractor.service;

import com.spbstu.common.dto.CurrencyDTO;
import java.util.List;
import reactor.core.publisher.Mono;

/**
 * Общий интерфейс для получения курса валют
 */
public interface CurrencyExtractor {
    /**
     * Вытащить данные с api ресурса
     *
     * @return ДТО
     */
    Mono<List<CurrencyDTO>> retrieveCurrency();
}
