package com.spbstu.currencyextractor.service;

import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import java.time.Instant;

public interface CurrencyHistoryExtractor {
    void extractHistory(HistoryTimeInterval period, Currency currency, Instant from, Instant to);
}
