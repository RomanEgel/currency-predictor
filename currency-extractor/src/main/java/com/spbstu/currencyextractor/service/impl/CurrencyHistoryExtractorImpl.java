package com.spbstu.currencyextractor.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import com.spbstu.common.util.JsonMapperUtil;
import com.spbstu.currencyextractor.service.CurrencyHistoryExtractor;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class CurrencyHistoryExtractorImpl implements CurrencyHistoryExtractor {
    @Value("${api.historyUri}")
    private String historyURI;
    @Value("${api.historyApiKey}")
    private String historyApiKey;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final Logger LOG = LoggerFactory.getLogger(CurrencyExtractorImpl.class);
    private static final WebClient WEB_CLIENT = WebClient.create();
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public void extractHistory(HistoryTimeInterval period, Currency currency, Instant from, Instant to) {
        final String interval;
        final ChronoUnit chronoUnit;
        final long minusTime;
        switch (period) {
            case MINUTE:
                interval = "5m";
                chronoUnit = ChronoUnit.MINUTES;
                minusTime = 60;
                break;
            case HOUR:
                interval = "1h";
                chronoUnit = ChronoUnit.HOURS;
                minusTime = 32;
                break;
            case DAY:
                interval = "1d";
                chronoUnit = ChronoUnit.DAYS;
                minusTime = 30;
                break;
            case WEEK:
                interval = "1w";
                chronoUnit = ChronoUnit.DAYS;
                minusTime = 90;
                break;
            case MONTH:
                interval = "month";
                chronoUnit = ChronoUnit.DAYS;
                minusTime = 365;
                break;
            default:
                throw new IllegalArgumentException("Интервал выбора истории не указан");
        }

        final String fromDate = DATE_FORMAT.format(from == null ? Date.from(Instant.now()) :
                Date.from(from));
        final String toDate = DATE_FORMAT.format(from == null ? Date.from(Instant.now().minus(minusTime, chronoUnit)) :
                Date.from(from));

        final String url = String.format(historyURI, currency.getCurrencyString(), interval, historyApiKey);
        WEB_CLIENT
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .subscribe(data -> {
                    final List<Map<String, Object>> listCur = new ArrayList<>();
                    final JsonNode json = JsonMapperUtil.stringToObject(data, JsonNode.class);
                    json.get("response").elements().forEachRemaining(c -> {
                        final Map<String, Object> curParams = new HashMap<>();
                        curParams.put("a_currency", currency.getCurrencyString());
                        curParams.put("a_value", c.get("h").asDouble());
                        final long epochLength = c.get("t").asText().length();
                        curParams.put("a_time", Date.from(epochLength <= 10 ? Instant.ofEpochSecond(c.get("t").asLong()) : Instant.ofEpochMilli(c.get("t").asLong())));
                        listCur.add(curParams);
                    });
                    new SimpleJdbcInsert(jdbcTemplate)
                            .withSchemaName("public")
                            .withTableName("currency_history_" + period.toString().toLowerCase())
                            .usingGeneratedKeyColumns("a_id")
                            .executeBatch(SqlParameterSourceUtils.createBatch(listCur));
                });
    }

}
