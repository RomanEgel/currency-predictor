package com.spbstu.currencyextractor.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.common.util.CurrencyUtil;
import com.spbstu.currencyextractor.service.CurrencyExtractor;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@NoArgsConstructor
@Getter
public class CurrencyExtractorImpl implements CurrencyExtractor {
    @Value("${api.realtimeUri}")
    private String resourceUri;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final Logger LOG = LoggerFactory.getLogger(CurrencyExtractorImpl.class);
    private static final WebClient WEB_CLIENT = WebClient.create();


    @Override
    public Mono<List<CurrencyDTO>> retrieveCurrency() {
        return WEB_CLIENT
                .get()
                .uri(resourceUri)
                .retrieve()
                .bodyToMono(JsonNode.class)
                .flatMap(list -> {
                    final List<CurrencyDTO> applicableCurrencies = new ArrayList<>();
                    list.get("forexList").elements().forEachRemaining(node -> {
                        if (CurrencyUtil.isCurrencySupported(node.get("ticker").asText())) {
                            final CurrencyDTO currencyDTO = new CurrencyDTO();
                            currencyDTO.setCurrency(CurrencyUtil.getCurrency(node.get("ticker").asText()));
                            currencyDTO.setValue(node.get("ask").asDouble());
                            String date = node.get("date").asText();
                            LOG.info("DATE IS " + date);
                            currencyDTO.setTime(Instant.from(LocalDateTime.from(dateTimeFormatter.parse(date)).toInstant(ZoneOffset.UTC)));
                            applicableCurrencies.add(currencyDTO);
                        }
                    });
                    return Mono.just(applicableCurrencies);
                })
                .log();
    }
}
