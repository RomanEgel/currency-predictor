package com.spbstu.currencyextractor.config;

import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.common.service.FluxKafkaSender;
import com.spbstu.common.service.impl.FluxKafkaSenderImpl;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

@Configuration
@EnableScheduling
@EnableSwagger2WebFlux
public class CurrencyExtractorConfig {
    @Value("${kafka.currencyTopic}")
    private String currencyTopicName;

    @Bean
    public KafkaSender<String, Object> currencyTopicSender() {
        final Map<String, Object> producerProps = new HashMap<>();
        producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        SenderOptions<String, Object> senderOptions = SenderOptions.create(producerProps);

        return KafkaSender.create(senderOptions);
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public FluxKafkaSender<CurrencyDTO> currencyKafkaSender(KafkaSender<String, Object> kafkaSender) {
        return new FluxKafkaSenderImpl<>(kafkaSender, currencyTopicName);
    }

    @Bean
    public ScheduledExecutorService currencyExtractTaskPoolExecutor() {
        return Executors.newSingleThreadScheduledExecutor();
    }
}
