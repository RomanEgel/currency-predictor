package com.spbstu.currencyextractor.rest;

import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import com.spbstu.currencyextractor.service.CurrencyHistoryExtractor;
import java.time.Instant;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class StartCurrencyHistoryExtractionController {
    private CurrencyHistoryExtractor historyExtractor;

    @GetMapping(value = "/receive-currency")
    public void receiveCurrency(@RequestParam HistoryTimeInterval interval, @RequestParam Currency currency,
                                @RequestParam(required = false) Instant from, @RequestParam(required = false) Instant to) {
        historyExtractor.extractHistory(interval, currency, from, to);
    }
}
