package com.spbstu.authservice.configs;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.savedrequest.RequestCacheAwareFilter;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Autowired
    private CorsConfigurationSource corsConfigurationSource;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        HttpSecurity httpSecurity = http.csrf().disable();
        httpSecurity
                .cors()
                .configurationSource(corsConfigurationSource)
                .and()
                .addFilterBefore(new SystemTokenFilter("auth-header", "CwFNHNJaXqumEFb57Vh9tJ667a3wJXd8"), RequestCacheAwareFilter.class)
                .authorizeRequests()
                .anyRequest()
                .authenticated();
    }

    /**
     * Фильтр для авторизации system token
     */
    private static final class SystemTokenFilter extends OncePerRequestFilter {
        private final String authHeaderName;
        private final String signKey;
        private final AuthenticationEventPublisher eventPublisher = new DefaultAuthenticationEventPublisher();

        private SystemTokenFilter(String authHeaderName, String signKey) {
            this.authHeaderName = authHeaderName;
            this.signKey = signKey;
        }

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                throws ServletException, IOException {
            final String jwtToken = request.getHeader(authHeaderName);
            if (StringUtils.isNotBlank(jwtToken)) {
                final Authentication authentication = convertJWT(jwtToken, signKey);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                if (Objects.nonNull(authentication)) {
                    eventPublisher.publishAuthenticationSuccess(authentication);
                } else {
                    final String message
                            = String.format("%s was provided but user can not be extracted. Probably jwt token is not correct", authHeaderName);
                    eventPublisher.publishAuthenticationFailure(new BadCredentialsException(message), authentication);
                }
            }
            filterChain.doFilter(request, response);
        }
    }

    private static Authentication convertJWT(final String jwtToken, final String signKey) {
        final IdbJwtAccessTokenConverter tokenConverter = new IdbJwtAccessTokenConverter();
        final MacSigner macSigner = new MacSigner(signKey);
        tokenConverter.setSigner(macSigner);
        tokenConverter.setVerifier(macSigner);
        final Map<String, Object> decode = tokenConverter.decode(jwtToken);
        return tokenConverter.extractAuthentication(decode);
    }

    /**
     * Расширяет родителя, чтобы сделать доступным метод decode
     */
    private static final class IdbJwtAccessTokenConverter extends JwtAccessTokenConverter {
        @Override
        public Map<String, Object> decode(String token) {
            return super.decode(token);
        }
    }
}
