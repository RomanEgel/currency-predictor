package com.spbstu.gatewayservice.filters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import java.util.LinkedHashMap;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

/**
 * @author aryabokon
 * @since 18.05.18 11:40
 */
@Component
public class AuthTokenFilter extends ZuulFilter {
    private static final Logger LOG = LoggerFactory.getLogger(AuthTokenFilter.class);

    @Autowired
    private ObjectMapper mapper;

    private static final String authHeaderName = "auth-header";
    private static final String signKey = "CwFNHNJaXqumEFb57Vh9tJ667a3wJXd8";

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 99;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        final HttpServletRequest request = ctx.getRequest();
        final String authorizationHeader = request.getHeader("Authorization");
        if (StringUtils.isBlank(authorizationHeader) || !authorizationHeader.startsWith("Bearer")) {
            return null;
        }
        final String requestToken = authorizationHeader.replaceFirst("^Bearer ", "");
        final IdbJwtAccessTokenConverter converter = new IdbJwtAccessTokenConverter();
        final MacSigner key = new MacSigner(signKey);
        converter.setSigner(key);
        converter.setVerifier(key);
        final String jwtToken = converter.encode(new DefaultOAuth2AccessToken(requestToken), getOauth2Auth());
        ctx.addZuulRequestHeader(authHeaderName, jwtToken);
        return null;
    }

    private OAuth2Authentication getOauth2Auth() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof OAuth2Authentication) {
            final OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
            final OAuth2Request oAuth2Request = oAuth2Authentication.getOAuth2Request();
            final Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();
            if (Objects.nonNull(userAuthentication) && userAuthentication.getDetails() instanceof LinkedHashMap) {
                final LinkedHashMap details = (LinkedHashMap) userAuthentication.getDetails();
                if (Objects.nonNull(details) && Objects.nonNull(details.get("principal"))) {
                    try {
                        final String serializedPrincipal = mapper.writeValueAsString(details.get("principal"));
                        final UsernamePasswordAuthenticationToken newUserAuthentication
                                = new UsernamePasswordAuthenticationToken(serializedPrincipal,
                                userAuthentication.getCredentials(),
                                userAuthentication.getAuthorities());
                        newUserAuthentication.setDetails(userAuthentication.getDetails());
                        return new OAuth2Authentication(oAuth2Request, newUserAuthentication);
                    } catch (JsonProcessingException e) {
                        LOG.error("Cannot serialize principal", e);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Расширяет родителя, чтобы сделать доступным метод encode
     */
    private static class IdbJwtAccessTokenConverter extends JwtAccessTokenConverter {
        @Override
        public String encode(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
            return super.encode(accessToken, authentication);
        }
    }
}
