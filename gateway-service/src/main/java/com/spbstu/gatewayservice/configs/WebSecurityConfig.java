package com.spbstu.gatewayservice.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfigurationSource;

@Configuration
public class WebSecurityConfig extends ResourceServerConfigurerAdapter {
    @Autowired
    private CorsConfigurationSource corsConfigurationSource;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        final HttpSecurity httpSecurity = http.httpBasic().disable();
        httpSecurity
                .cors()
                .configurationSource(corsConfigurationSource)
                .and()
                .authorizeRequests()
                .antMatchers("/auth-service/**",  "/**/swagger-ui.html", "/currency-keeper/open-socket/*", "/currency-keeper/current", "/currency-keeper/current/*")
                .permitAll()
                .anyRequest()
                .authenticated();
    }
}
