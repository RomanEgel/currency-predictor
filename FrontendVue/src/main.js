import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
window.authhost = 'http://localhost:8082'
window.sockethost = 'http://localhost:8899'
window.predictionhost = 'http://localhost:5000'

new Vue({
  render: h => h(App),
}).$mount('#app')
