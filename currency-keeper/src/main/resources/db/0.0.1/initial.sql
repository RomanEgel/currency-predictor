create table if not exists currency_history_realtime
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_realtime_pkey
		primary key (a_id, a_time)
);

create table if not exists currency_history_minute
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_minute_pkey
		primary key (a_id, a_time)
);

create table if not exists currency_history_hour
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_hour_pkey
		primary key (a_id, a_time)
);

create table if not exists currency_history_day
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_day_pkey
		primary key (a_id, a_time)
);

create table if not exists currency_history_week
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_week_pkey
		primary key (a_id, a_time)
);

create table if not exists currency_history_month
(
	a_id bigserial not null,
	a_time timestamp not null,
	a_currency varchar not null,
	a_value numeric not null,
	constraint currency_history_month_pkey
		primary key (a_id, a_time)
);

create table if not exists currency
(
	id bigserial not null,
	name varchar
);