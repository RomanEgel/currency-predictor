package com.spbstu.currencykeeper.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.common.dto.HistoryTimeInterval;
import com.spbstu.common.util.JsonMapperUtil;
import com.spbstu.currencykeeper.service.HistoryCurrencyService;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HistoryCurrencyServiceImpl implements HistoryCurrencyService {
    private final JdbcTemplate jdbcTemplate;

    @Override
    public JsonNode getHistory(List<Currency> currencyList, HistoryTimeInterval interval, Date from, Date to, Double max, Double min) {
        final String tableName = "currency_history_" + interval.toString().toLowerCase();

        final String query = "select * from %s where a_currency in (%s) and a_time >= ? and a_time <= ?";

        final List<CurrencyDTO> result = jdbcTemplate.query(String.format(query, tableName,
                currencyList.stream()
                        .map(c -> "'" + c.getCurrencyString() + "'").collect(Collectors.joining(", "))),
                new Object[] {java.sql.Date.from(from.toInstant()), java.sql.Date.from(to.toInstant())},
                (row, num) -> {
                    final CurrencyDTO currencyDTO = new CurrencyDTO();
                    currencyDTO.setValue(row.getDouble("a_value"));
                    currencyDTO.setCurrency(getCurrency(row.getString("a_currency")));
                    currencyDTO.setTime(row.getTimestamp("a_time").toInstant());
                    return currencyDTO;
                });
        final ArrayNode resultNode = JsonMapperUtil.arrayNode();
        for (Currency currency : currencyList) {
            double maxValue = 0.0;
            double minValue = 1000000.0;
            double avg = 0.0;
            final ObjectNode currencyNode = JsonMapperUtil.objectNode();
            currencyNode.put("currency", currency.getCurrencyString().replace("/", "_"));
            final ArrayNode timeNode = JsonMapperUtil.arrayNode();
            final ArrayNode valueNode = JsonMapperUtil.arrayNode();
            final List<CurrencyDTO> currencyDTOS = result.stream().filter(c -> c.getCurrency().equals(currency)).collect(Collectors.toList());
            for (CurrencyDTO currencyDTO : currencyDTOS) {
                timeNode.add(currencyDTO.getTime().toString());
                valueNode.add(currencyDTO.getValue());
                avg += currencyDTO.getValue();
                if (maxValue < currencyDTO.getValue()) {
                    maxValue = currencyDTO.getValue();
                } else if (minValue > currencyDTO.getValue()) {
                    minValue = currencyDTO.getValue();
                }
            }
            avg = avg/currencyDTOS.size();
            currencyNode.set("date", timeNode);
            currencyNode.set("value", valueNode);

            if (maxValue/avg < 1 + max && minValue/avg > 1 - min) {
                resultNode.add(currencyNode);
            }
        }
        return resultNode;
    }

    private Currency getCurrency(String cur) {
        for (Currency value : Currency.values()) {
            if (value.getCurrencyString().equals(cur)) {
                return value;
            }
        }
        return null;
    }
}
