package com.spbstu.currencykeeper.service;

import com.spbstu.common.dto.CurrencyDTO;

public interface RealTimeCurrencyService {
    void saveRTCurrency(CurrencyDTO currencyDTO);
}
