package com.spbstu.currencykeeper.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import java.util.Date;
import java.util.List;

public interface HistoryCurrencyService {

    JsonNode getHistory(List<Currency> currencyList, HistoryTimeInterval interval, Date from, Date to, Double max, Double min);
}
