package com.spbstu.currencykeeper.service.impl;

import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.currencykeeper.service.RealTimeCurrencyService;
import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class RealTimeCurrencyServiceImpl implements RealTimeCurrencyService {
    private final JdbcTemplate jdbcTemplate;
    private final SimpMessagingTemplate webSocketMessage;

    @Override
    @Transactional
    public void saveRTCurrency(CurrencyDTO currencyDTO) {
        final Map<String, Object> params = new HashMap<>();
        params.put("a_time", Timestamp.from(currencyDTO.getTime()));
        params.put("a_currency", currencyDTO.getCurrency().getCurrencyString());
        params.put("a_value", currencyDTO.getValue());

        currencyDTO.setTime(currencyDTO.getTime().plus(6 ,ChronoUnit.HOURS));
        // отправляем по сокеты всем клиентам
        webSocketMessage.convertAndSend("/current", currencyDTO);

        new SimpleJdbcInsert(jdbcTemplate)
                .withSchemaName("public")
                .withTableName("currency_history_realtime")
                .usingGeneratedKeyColumns("a_id")
                .execute(params);
    }
}
