package com.spbstu.currencykeeper.kafka;

import com.spbstu.common.dto.CurrencyDTO;
import com.spbstu.common.util.JsonMapperUtil;
import com.spbstu.currencykeeper.service.RealTimeCurrencyService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CurrencyKafkaListener {
    private final RealTimeCurrencyService currencyService;
    private static final Logger LOG = LoggerFactory.getLogger(CurrencyKafkaListener.class);

    @KafkaListener(topics = "${kafka.currencyTopic}")
    public void receiveMessage(String payload) {
        final CurrencyDTO currencyDTO = JsonMapperUtil.stringToObject(payload, CurrencyDTO.class);
        LOG.info("receive message:" + currencyDTO);
        currencyService.saveRTCurrency(currencyDTO);
    }
}
