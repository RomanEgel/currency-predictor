package com.spbstu.currencykeeper.config;

import java.io.IOException;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CorsConfigurationSource corsConfigurationSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http/*.cors()
                .configurationSource(corsConfigurationSource)
                .and()*/
                .addFilterBefore(new JwtSecurityFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/open-socket/**", "/open-socket")
                .permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v2/api-docs",
                "/swagger-resources/configuration/ui",
                "/swagger-resources",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }


    /**
     * Фильтр для поиска токена JWT
     */

    private final class JwtSecurityFilter extends OncePerRequestFilter {

        @Override
        protected void doFilterInternal(HttpServletRequest request,
                                        HttpServletResponse response,
                                        FilterChain filterChain
        ) throws ServletException, IOException {
            final String jwtToken = request.getHeader("auth-header");
            if (jwtToken != null) {

                final Authentication idbAuthentication = convertJWT(jwtToken, "CwFNHNJaXqumEFb57Vh9tJ667a3wJXd8");
                SecurityContextHolder.getContext().setAuthentication(idbAuthentication);
            }
            filterChain.doFilter(request, response);
        }
    }

    private static Authentication convertJWT(final String jwtToken, final String signKey) {
        final IdbJwtAccessTokenConverter tokenConverter = new IdbJwtAccessTokenConverter();
        final MacSigner macSigner = new MacSigner(signKey);
        tokenConverter.setSigner(macSigner);
        tokenConverter.setVerifier(macSigner);
        final Map<String, Object> decode = tokenConverter.decode(jwtToken);
        return tokenConverter.extractAuthentication(decode);
    }

    /**
     * Расширяет родителя, чтобы сделать доступным метод decode
     */
    private static final class IdbJwtAccessTokenConverter extends JwtAccessTokenConverter {
        @Override
        public Map<String, Object> decode(String token) {
            return super.decode(token);
        }
    }
}