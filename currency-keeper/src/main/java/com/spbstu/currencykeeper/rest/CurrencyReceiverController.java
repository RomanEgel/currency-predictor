package com.spbstu.currencykeeper.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import com.spbstu.currencykeeper.service.HistoryCurrencyService;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CurrencyReceiverController {
    private HistoryCurrencyService historyCurrencyService;

    @GetMapping("/history")
    public JsonNode getHistory(@RequestParam("currencyList") List<Currency> currencyList,
                               @RequestParam("from") @DateTimeFormat(pattern="yyyy-MM-dd") Date from,
                               @RequestParam("to") @DateTimeFormat(pattern="yyyy-MM-dd") Date to,
                               @RequestParam("max") Double max,
                               @RequestParam("min") Double min) {

        return historyCurrencyService.getHistory(currencyList, HistoryTimeInterval.WEEK, from, to, max, min);
    }

    @GetMapping("/tuta")
    public String getTuta() {
        return "tuta";
    }
}
