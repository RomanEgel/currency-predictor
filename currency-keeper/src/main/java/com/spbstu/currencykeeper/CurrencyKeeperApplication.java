package com.spbstu.currencykeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyKeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrencyKeeperApplication.class, args);
    }

}
