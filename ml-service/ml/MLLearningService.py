import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sklearn import preprocessing
from sklearn import utils
from ml import DBService
from ml import views
from sklearn.model_selection import train_test_split
from scipy.optimize import least_squares
import math

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LassoCV
from sklearn.pipeline import make_pipeline

def func_e(x, a, b, c,  a0, a1, a2, a3):
    return c - np.exp(-0.000001*a*x+0.000001*b) + (a3*x**3 + a2*x**2 + a1*x + a0)

def func(x, b, c):
    return  b * x + c

def func_2(x,  a0, a1, a2):
    return a2*x**2 + a1*x + a0

def func_3(x,  a0, a1, a2, a3):
    return a3*x**3 + a2*x**2 + a1*x + a0

def func_4(x,  a0, a1, a2, a3, a4):
    return a4*x**4+ a3*x**3 + a2*x**2 + a1*x + a0

def func_5(x,  a0, a1, a2, a3, a4, a5):
    return a5*x**5 + a4*x**4+ a3*x**3 + a2*x**2 + a1*x + a0

def func_6(x,  a0, a1, a2, a3, a4, a5, a6):
    return a6*x**6  + a5*x**5 + a4*x**4+ a3*x**3 + a2*x**2 + a1*x + a0

def func_7(x,  a0, a1, a2, a3, a4, a5, a6, a7):
    return a7*x**7 +  a6*x**6 + a5*x**5 + a4*x**4 + a3*x**3 + a2*x**2 + a1*x + a0

def learnModelForInterval(fucn_for_data, filename):
    ds = fucn_for_data
    print(ds)
    X = []
    y = []
    X_ = []
    y_ = []
    for each in ds:
        X.append(each[0])
        y.append(each[1])
    X =  np.array(X)
    y =  np.array(y)

    X =  np.array(X)
    y =  np.array(y)
    print(X)
    print(y)
    X_train = X[0:80]
    y_train = y[0:80]
    X_test = X[79:100]
    y_test = y[79:100]

    
    print(X_train, X_test )
##    plt.plot(X_train, y_train, 'b-', label='data')
##    plt.plot(X_test, y_test, 'r-', label='test')
    
    scores = []
    interval = filename.split('_')[0]
    if interval == 'year' or interval == 'month':
        funcs = [func]
    elif interval == 'week' or interval == 'day':
        funcs = [func_e]
    else:
        funcs = [func_4]
        
        

    for fun in funcs:
        popt, pcov = curve_fit(fun, X_train, y_train)
        
        #score X_train
        y_real =  fun(X_train, *popt)
        y_delta = (y_real-y_train)**2
        score = (1 - ((y_delta.sum())/(len(y_real)-1))**(1/2))*100
        print(filename, "  ", score)
        scores.append(score)
        if filename.split('_')[0] == "realtime":
            conn=views.getConnection()
            DBService.insert_score(conn, filename, score)

##        plt.plot(X_train, y_train, 'b-', label='data')
##        plt.plot(X_test, y_test, 'r-', label='test')
##        plt.plot(X_train, fun(X_train, *popt), "--g", label='curve')
##        plt.plot(X_test, fun(X_test, *popt), "--g", label='curve')
##        
##        plt.xlabel('x')
##        plt.ylabel('y')
##        plt.legend()
##        plt.show()
        

        max_score_model = (scores.index(max(scores)))                   

        fun = funcs[max_score_model]
        
        popt, pcov = curve_fit(fun, X_train, y_train)
        f = open(filename, 'w')
        for element in popt:
            f.write(str(element))
            f.write(" ")
        f.close()

        plt.plot(X_train, y_train, 'b-', label='data')
        plt.plot(X_test, y_test, 'r-', label='test')
        plt.plot(X_train, fun(X_train, *popt), "--g", label='curve')
        plt.plot(X_test, fun(X_test, *popt), "--g", label='curve')
        
        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend()
        plt.show()
    
    print("SCORE", scores[max_score_model])




   
