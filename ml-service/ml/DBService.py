import psycopg2
from psycopg2 import sql
from config import config
import datetime
import numpy as np
 
def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        params = config()
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return conn

def select_min(conn, currency, interval):
    print("CUR ", currency, "intr ", interval)
    cur = conn.cursor()
    cur.execute("select min_d from min_date where currency=%s and interval=%s", (currency, interval))
    row = cur.fetchone()
    return row[0]

def insert_score(conn, interval_cur, score):
    cur = conn.cursor()
    #cur.execute("insert into score_models(interval_cur, score) values(%s, %s)", (interval_cur, score))
    cur.execute("update score_models set score=%s where interval_cur=%s", (score, interval_cur))
    conn.commit()
    cur.close()

def get_score(conn, interval_cur):
    print("interval_cur: " + interval_cur)
    cur = conn.cursor()
    cur.execute("select cast(score as float) from score_models where interval_cur=%s", (interval_cur, ))
    row = cur.fetchone()
    return row[0]
    
def formModelForMinute(conn, currency, table_name):
    try:
        print(currency, table_name)
        cur = conn.cursor()
        cur.execute(sql.SQL("select date_, cur_ from (select a_time, (EXTRACT(EPOCH from a_time)) as date_, cast(a_value as float) as cur_ from {t} where a_currency=%s order by date_ desc limit 100) as t order by date_;")
                            .format(t=sql.Identifier(table_name)), (currency, ))

        data_ = []
        currency_ = []

        min_table = ""
        if table_name == "currency_history_month":
            min_table = "year"
        elif table_name == "currency_history_week":
            min_table = "month"
        elif table_name == "currency_history_day":
            min_table = "week"
        elif table_name == "currency_history_hour":
            min_table = "day"
        elif table_name == "currency_history_realtime":
            min_table = "realtime"
        
        print("TABLE", min_table)
        
        row = cur.fetchone()
        while row is not None:
            data_.append(row[0])
            currency_.append(row[1])
            row = cur.fetchone()
        cur.close()

        print("data: ",  data_)
        print(currency_)

        data_min = min(data_)
        cur = conn.cursor()
        cur.execute("update min_date set min_d=%s where interval=%s and currency=%s", (data_min, min_table, currency))
        conn.commit()
        cur.close()
        
        print("MIN", data_min)
        data_new = []
        for each in data_:
            data_new.append((each-data_min)/100)

        result = []
        for i in np.arange(0, len(data_new), 1):
            res = []
            res.append(data_new[i])
            res.append(currency_[i])
            result.append(res)
            
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return result






