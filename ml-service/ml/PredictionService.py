import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sklearn import preprocessing
from sklearn import utils
from ml import DBService
from ml import views
from ml import MLLearningService
import time
import json

def predictForInterval(currency, interval): 
    f = open(interval  + "_" +  currency, 'r')
    coef = f.read()
    coef =  np.array(coef.split())
    f.close()
    coef_ = []
    for each in coef:
        coef_.append(float(each))


    delta = 0
    if interval == "year":
        delta = 62208 # на 12 месяцев
        now = 117504
        func = MLLearningService.func
    elif interval == "month":
        delta = 5184 # 1 мес
        now = 117504
        func = MLLearningService.func
    elif interval == "week":
        delta = 1210 # 7 дней
        now = 99360
        func = MLLearningService.func_e
    elif interval == "day":
        delta = 173 #на 24 часа
        now = 3564
        func = MLLearningService.func_e
    elif interval == "realtime":
        delta = 0.12 # 1 минута
        now = 2.97
        func = MLLearningService.func_4
        
    conn=views.getConnection()
    min_data = DBService.select_min(conn, currency.upper()+ "/JPY", interval)

    x = []
    x.append(now)
    now_old = x[0]
    now_future = x[0]
    
    if interval != "realtime":
        j = 10
        while j > 0:
            x.append(now_old - delta)
            now_old =  now_old - delta
            j = j - 1
    
    i = 5
    while i > 0:
        x.append(now_future + delta)
        now_future =  now_future + delta
        i = i - 1

    x = sorted(x)


    

    y = []
    for each in x:
        y.append(func(each, *coef_))

    x_ = []
    for each in x:
        x_.append(time.ctime(100 * each + min_data))

    x = x_


    date = []
    currency = []
    for i in np.arange(0, len(x), 1):
        date.append(x[i])
        currency.append(y[i])
    result = {}
    result["date"] = date
    result["currency"] = currency
    result = json.dumps(result)
    print(result)
    return result
    


   
