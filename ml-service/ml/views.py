from ml import ml
from ml import DBService
from ml import PredictionService
from flask import request
from flask import Flask
from flask_cors import CORS, cross_origin

conn = DBService.connect()

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'application/json'

def getConnection():
    return conn

@ml.route('/predict')
@ml.route('/index')
@cross_origin()
def index():
    try:
        interval = request.args.get('interval')
        currency = request.args.get('currency')
        print(currency)
        print(interval)
        filename =  interval  + "_" +  currency
        print(filename)
        res = PredictionService.predictForInterval(currency, interval)
        a = str(res)
        return a
    except (Exception) as error:
        return 'Bad request!', 400

@ml.route('/score')
@ml.route('/index1')
@cross_origin()
def index1():
    try:
        currency = request.args.get('currency')
        filename =  "realtime"  + "_" +  currency
        res = DBService.get_score(conn, filename)
        return str(res)
    except (Exception) as error:
        return 'Bad request!', 400


