from ml import ml
from ml import MLLearningService
from ml import DBService
from ml import views
from threading import Thread
import schedule
import time

conn = views.getConnection()

def learnModelforYear_usd():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'USD/JPY', 'currency_history_month'), "year_usd")
    return
def learnModelforMonth_usd():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'USD/JPY', 'currency_history_week'), "month_usd")
    return
def learnModelforWeek_usd():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'USD/JPY', 'currency_history_day'), "week_usd")
    return
def learnModelforDay_usd():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'USD/JPY', 'currency_history_hour'), "day_usd")
    return
def learnModelforRealtime_usd():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'USD/JPY', 'currency_history_realtime'), "realtime_usd")
    return

def learnModelforYear_eur():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'EUR/JPY', 'currency_history_month'), "year_eur")
    return
def learnModelforMonth_eur():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'EUR/JPY', 'currency_history_week'), "month_eur")
    return
def learnModelforWeek_eur():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'EUR/JPY', 'currency_history_day'), "week_eur")
    return
def learnModelforDay_eur():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'EUR/JPY', 'currency_history_hour'), "day_eur")
    return
def learnModelforRealtime_eur():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'EUR/JPY', 'currency_history_realtime'), "realtime_eur")
    return

def learnModelforYear_gbp():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'GBP/JPY', 'currency_history_month'), "year_gbp")
    return
def learnModelforMonth_gbp():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'GBP/JPY', 'currency_history_week'), "month_gbp")
    return
def learnModelforWeek_gbp():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'GBP/JPY', 'currency_history_day'), "week_gbp")
    return
def learnModelforDay_gbp():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'GBP/JPY', 'currency_history_hour'), "day_gbp")
    return
def learnModelforRealtime_gbp():
    MLLearningService.learnModelForInterval(DBService.formModelForMinute(conn, 'GBP/JPY', 'currency_history_realtime'), "realtime_gbp")
    return


def f():
    schedule.every().second.do(learnModelforYear_usd)#monday
    schedule.every().second.do(learnModelforMonth_usd)#monday
    schedule.every().second.do(learnModelforWeek_usd)#monday
    schedule.every().second.do(learnModelforDay_usd)#monday
    schedule.every().second.do(learnModelforRealtime_usd)#minute

    schedule.every().second.do(learnModelforYear_eur)
    schedule.every().second.do(learnModelforMonth_eur)
    schedule.every().second.do(learnModelforWeek_eur)
    schedule.every().second.do(learnModelforDay_eur)
    schedule.every().second.do(learnModelforRealtime_eur)

    schedule.every().second.do(learnModelforYear_gbp)
    schedule.every().second.do(learnModelforMonth_gbp)
    schedule.every().second.do(learnModelforWeek_gbp)
    schedule.every().second.do(learnModelforDay_gbp)
    schedule.every().second.do(learnModelforRealtime_gbp)
    
    while True:
        schedule.run_pending()

thread1 = Thread(target=f)
thread1.start()
ml.run(debug = False)


