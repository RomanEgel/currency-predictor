package com.spbstu.registryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


/**
 * Стартовый класс сервиса
 */
@SpringBootApplication
@EnableEurekaServer
public class RegistryServiceApplication {

    /**
     * Стартовый метод сервиса
     * @param args аргрументы командной строки
     */
    public static void main(String[] args) {
        SpringApplication.run(RegistryServiceApplication.class, args);
    }

}
