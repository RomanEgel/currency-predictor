package com.spbstu.common.service.impl;

import com.spbstu.common.service.FluxKafkaSender;
import com.spbstu.common.util.JsonMapperUtil;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;

@RequiredArgsConstructor
public class FluxKafkaSenderImpl<T> implements FluxKafkaSender<T> {
    private final KafkaSender<String, Object> kafkaSender;
    private final String topicName;
    private static final Logger LOGGER = LoggerFactory.getLogger(FluxKafkaSender.class);

    @Override
    public void send(Flux<T> flux) {
        kafkaSender
                .send(flux.map(data -> SenderRecord.create(new ProducerRecord<>(topicName, JsonMapperUtil.objectToString(data)), 1)))
                .then()
                .doOnError(e -> LOGGER.error("error while sending data to kafka", e))
                .subscribe();
    }
}
