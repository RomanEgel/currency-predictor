package com.spbstu.common.service;

import reactor.core.publisher.Flux;

public interface FluxKafkaSender<T> {
     void send(Flux<T> flux);
}
