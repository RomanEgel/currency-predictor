package com.spbstu.common.util;

import com.spbstu.common.dto.Currency;
import com.spbstu.common.dto.HistoryTimeInterval;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class CurrencyUtil {
    private static final Map<HistoryTimeInterval, String> tableNameMap = new HashMap<>();

    static {
        tableNameMap.put(HistoryTimeInterval.MINUTE, "currency_history_minute");
        tableNameMap.put(HistoryTimeInterval.HOUR, "currency_history_hour");
        tableNameMap.put(HistoryTimeInterval.DAY, "currency_history_day");
        tableNameMap.put(HistoryTimeInterval.WEEK, "currency_history_week");
        tableNameMap.put(HistoryTimeInterval.MONTH, "currency_history_month");
    }

    public static boolean isCurrencySupported(String name) {
        return "USD/JPY".equals(name) || "EUR/JPY".equals(name) || "GBP/JPY".equals(name);
    }

    public static Currency getCurrency(String name) {
        for (Currency value : Currency.values()) {
            if (value.getCurrencyString().equals(name)) {
                return value;
            }
        }
        throw new NoSuchElementException(String.format("Currency with name %s does not supported", name));
    }

    public static String getTableNameByPeriod(HistoryTimeInterval interval) {
        return tableNameMap.get(interval);
    }
}
