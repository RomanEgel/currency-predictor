package com.spbstu.common.dto;

import lombok.Getter;

/**
 * Типы валют
 */
@Getter
public enum Currency {

    /**
     * Доллар -> Японская йена
     */
    USD_JPY("USD/JPY"),

    /**
     * Евро -> Японская йена
     */
    EUR_JPY("EUR/JPY"),

    /**
     * Фунт -> Японская йена
     */
    GBP_JPY("GBP/JPY");

    private String currencyString;


    Currency(String currencyString) {
        this.currencyString = currencyString;
    }
}
