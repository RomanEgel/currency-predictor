package com.spbstu.common.dto;

public enum HistoryTimeInterval {
    MONTH,

    WEEK,

    DAY,

    HOUR,

    MINUTE
}
