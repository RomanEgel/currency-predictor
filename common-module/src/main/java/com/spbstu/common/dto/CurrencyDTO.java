package com.spbstu.common.dto;

import java.io.Serializable;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ДТО валюты для реально времени
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyDTO implements Serializable {
    private static final long serialVersionUID = 7293586405169605717L;
    private double value;
    private Currency currency;
    private Instant time;

    @Override
    public String toString() {
        return "CurrencyDTO{" +
                "value=" + value +
                ", currency=" + currency +
                ", time=" + time +
                '}';
    }
}
